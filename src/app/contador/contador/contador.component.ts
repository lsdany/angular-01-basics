/**
 * Export enables the use in other place
 */

import {Component} from '@angular/core';

@Component({
    selector: 'app-contador',
    template: `
        <h1> Hola Mundo !</h1>
        <h1> {{title}}</h1>


        <h3>La base es: <strong>{{base}}</strong></h3>

        <button (click)="add(base)"> + {{base}} </button> <!--Lo de dentro es codigo js-->
        <span>{{num}}</span>
        <button (click)="substract(base)"> - {{base}} </button>`
})
export class ContadorComponent {


    title: string = 'bases';
    num: number = 10;
    base: number = 5;

    sum() {
        this.num += 1;
    }

    subs() {
        this.num -= 1;
    }

    add(val: number) {
        this.num += val;
    }

    substract(val: number) {
        this.num -= val;
    }


}