import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-listado',
    templateUrl: './listado.component.html',
    styleUrls: ['./listado.component.css']
})
export class ListadoComponent {

    heroes: string[] = ['Luke', 'Leia', 'Obi-Wan', 'Anakin'];

    hBorrado: string = '';

    borrar() {
        console.log('borrando');
        const h = this.heroes.shift();
        console.log(h);
        this.hBorrado = h || ''; //h o un string vacio si es undefined
    }

}
